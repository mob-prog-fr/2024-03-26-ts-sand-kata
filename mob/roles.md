# La traducteurice

C'est la personne au clavier. 

Son rôle c'est de traduire au mieux de ses capacités les idées de la navigateurice. 

Vous êtes novice dans le langage ou le développement ? C'est le poste parfait. On s'engage à ne pas vous juger et à vous donner toutes les clés pour écrire confortablement.

À ce poste :

   * __Aucune initiative__, tout ce que vous écrivez DOIT avoir été demandé par la navigateurice
   * Vous ne parlez que pour __demander des clarifications__
   * __Lâchez le clavier dès la sonnerie retentit__, même si vous êtes au milieu d'un truc


# La navigateurice

C'est la personne qui va aller __demander leurs idées au équipier et équipières__. 

Prenez une idée et donnez là au __plus haut niveau d'abstraction possible__ à la traducteurice.

Par exemple :

   * L'équipe pense qu'on a fini la règle Fizz
   * « @traducteurice, peux-tu écrire un test pour le cas Buzz ? »
   * Si ce n'est pas assez clair : « Écris un test qui vérifie que quand on appelle la fonction avec le nombre 5 on retourne "Buzz" »
   * Si ce n'est pas assez clair : « Tu peux dupliquer le bloc de code entre la ligne 17 et 23, et changer les valeurs ligne 18 et 22 »
   * etc.


# Les équipiers et équipières

C'est le rôle le plus important. C'est vous le moteur de l'équipe. 

On favorise les __décisions rapides__ pour dynamiser le mob. Alors __poussez fort derrière les idées__, les bonnes comme les mauvaises. C'est mieux pour l'équipe d'aller rapidement dans un mur que de démonter les idées en débattant longuement. 

__On ne cherche pas le code parfait__, juste le code fait ensemble.

À ce poste

   * Vous devez __lever la main pour parler__, seule la navigateurice peut vous donner la parole
   * Vous avez le droit de poser des questions jusqu'à ce que vous compreniez ce qui se passe
       * on peut aussi vous répondre dans le chat
   * Vous devez veillez à la __répartition équitable de la parole__
       * Ne monopolisez pas la parole
       * __Plus vous avez de privilèges plus vous devez laisser de la place__.
         * Les privilèges sont des avantages sociaux qui peuvent nous amener à bénéficier d’une mise en avant et d’un confort, voire une posture de pouvoir par rapport à d’autres personnes. Exemples : si je suis dev senior par rapport à des personnes plus junior, si je suis valide par rapport à des personnes en situation de handicap, etc
       * Les seniors sont là pour débloquer l'équipe, pas pour infliger de l'aide
       * Exemple : Je suis un·e dev expérimenté·e avec une super idée. Il vaut mieux laisser un·e junior proposer quelque chose.
       * il vaut mieux un long silence clôt par un ou une dev junior, qu'une équipe qui ne donne sa place qu'aux devs seniors
   * Aidez l'équipe à se débloquer, google, chatgpt, doc officielle, dégainez tout
   * __Partez quand vous voulez, faites des pauses quand vous voulez__. (Règle des deux pieds) 